import javax.persistence.*;

import java.util.List;

import static org.junit.Assert.*;

public class BuddyInfoTest {
    public void performJPA(){
        BuddyInfo b1 = new BuddyInfo();
        b1.setId(1);
        b1.setName("Bhavik");
        b1.setPhoneNumber("1234949452");
        BuddyInfo b2 = new BuddyInfo();
        b2.setId(2);
        b2.setName("Taz");
        b2.setPhoneNumber("7584840303");
        BuddyInfo b3 = new BuddyInfo();
        b3.setId(3);
        b3.setName("Nisaa");
        b3.setPhoneNumber("3030920011");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AddressBook");

        EntityManager em = emf.createEntityManager();

        EntityTransaction tx = em.getTransaction();

        tx.begin();

        em.persist(b1);
        em.persist(b2);
        em.persist(b3);

        tx.commit();

        Query q = em.createQuery("SELECT b from BuddyInfo b");

        @SuppressWarnings("unchecked")
        List<BuddyInfo> buddies = q.getResultList();

        for (BuddyInfo b: buddies) {
            System.out.println(b);
        }

        em.close();

        emf.close();
    }
}