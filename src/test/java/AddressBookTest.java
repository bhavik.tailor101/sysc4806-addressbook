import org.junit.Before;
import org.junit.Test;

import javax.persistence.*;

import java.util.List;

import static org.junit.Assert.*;

public class AddressBookTest {
    AddressBook testAb = new AddressBook();

    @Before
    public void setup() {
        BuddyInfo b1 = new BuddyInfo("Bhavik", "5794285000");
        BuddyInfo b2 = new BuddyInfo("Taz", "7584840303");
        BuddyInfo b3 = new BuddyInfo("Nisaa", "3030920011");

        testAb.getAddressBook().add(b1);
        testAb.getAddressBook().add(b2);
        testAb.getAddressBook().add(b3);
    }

    @Test
    public void testAddToAddressBook() {
        AddressBook actualAb = new AddressBook();

        BuddyInfo b1 = new BuddyInfo("Bhavik", "5794285000");
        BuddyInfo b2 = new BuddyInfo("Taz", "7584840303");
        BuddyInfo b3 = new BuddyInfo("Nisaa", "3030920011");

        actualAb.addToAddressBook(b1);
        actualAb.addToAddressBook(b2);
        actualAb.addToAddressBook(b3);

        assert(actualAb.equals(testAb));
    }

    public void performJPA() {
        AddressBook ab = new AddressBook();

        BuddyInfo b1 = new BuddyInfo();
        b1.setId(1);
        b1.setName("Bhavik");
        b1.setPhoneNumber("1234949452");
        BuddyInfo b2 = new BuddyInfo();
        b2.setId(2);
        b2.setName("Taz");
        b2.setPhoneNumber("7584840303");


        ab.addToAddressBook(b1);
        ab.addToAddressBook(b2);


        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AddressBook");

        EntityManager em = emf.createEntityManager();

        EntityTransaction tx = em.getTransaction();

        tx.begin();

//        em.persist(b1);
//        em.persist(b2);
//        em.persist(b3);

        em.persist(ab);

        tx.commit();

        Query q = em.createQuery("SELECT a from AddressBook a");
        @SuppressWarnings("unchecked")
        List<AddressBook> addressBookList = q.getResultList();

        for (AddressBook a: addressBookList) {
            System.out.println(a);
        }

        em.close();

        emf.close();

    }

}