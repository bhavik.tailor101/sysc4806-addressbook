import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Launcher {
    public void launch() {
        String[] contextPaths = new String[] {"META-INF/buddy-context.xml"};
        new ClassPathXmlApplicationContext(contextPaths);
    }
}
