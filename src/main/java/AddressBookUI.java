import ui.ActionListenerButton;
import ui.FlowLayoutPanel;
import ui.MainFrame;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class AddressBookUI {
    public void buildUI() {
        AddressBook ab = new AddressBook();
        MainFrame mf = new MainFrame();
        mf.setTitle("Address Book");
        FlowLayoutPanel panel = new FlowLayoutPanel();
        List components = new ArrayList();
        JTextArea textArea = new JTextArea();
        textArea.setRows(20);
        textArea.setColumns(50);
        AddNewBuddyActionListener buddyAdd = new AddNewBuddyActionListener();
        buddyAdd.setAddressBook(ab);
        buddyAdd.setTextArea(textArea);
        ActionListenerButton addButton = new ActionListenerButton();
        addButton.addActionListener(buddyAdd);
        components.add(addButton);
        addButton.setText("Add New Buddy");
        components.add(textArea);
        panel.setPanelComponents(components);
        panel.init();
        mf.add(panel);
        mf.init();
    }

    public static void main(String[] args) {
        AddressBookUI ui = new AddressBookUI();
        ui.buildUI();
    }
}
