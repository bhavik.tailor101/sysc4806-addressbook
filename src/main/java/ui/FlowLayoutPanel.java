package ui;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class FlowLayoutPanel extends JPanel {
    private List panelComponents;

    public void setPanelComponents(List components) {
        this.panelComponents = components;
    }

    public void init() {
        setLayout(new FlowLayout());

        for (Object o: panelComponents) {
            add((Component) o);
        }
    }
}
