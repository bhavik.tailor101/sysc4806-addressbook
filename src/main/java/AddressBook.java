import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
public class AddressBook {
    @Id
    private int id;

    @OneToMany(mappedBy = "addressBook", cascade = CascadeType.PERSIST)
    private List<BuddyInfo> addressBook;

    public AddressBook() {
        this.addressBook = new ArrayList();
    }

    public void addToAddressBook(BuddyInfo b) {
        getAddressBook().add(b);
    }


    public List<BuddyInfo> getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(List<BuddyInfo> addressBook) {
        this.addressBook = addressBook;
    }

    @Override
    public String toString() {
        String results = "";
        for(BuddyInfo b : getAddressBook()) {
            results += b.toString();
        }
        return results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressBook)) return false;
        AddressBook a = (AddressBook) o;
        return a.getAddressBook().equals(this.getAddressBook());
    }

    public static void main(String[] args) {
        Launcher launcher = new Launcher();
        launcher.launch();

        BuddyInfo b1 = new BuddyInfo("Bhavik", "1234949452");
        BuddyInfo b2 = new BuddyInfo("Taz", "7584840303");
        BuddyInfo b3 = new BuddyInfo("Nisaa", "3030920011");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AddressBook");

        EntityManager em = emf.createEntityManager();

        EntityTransaction tx = em.getTransaction();

        tx.begin();

        em.persist(b1);
        em.persist(b2);
        em.persist(b3);

        tx.commit();

        Query q = em.createQuery("SELECT b from BuddyInfo b");

        @SuppressWarnings("unchecked")
        List<BuddyInfo> buddies = q.getResultList();

        for (BuddyInfo b: buddies) {
            System.out.println(b + "\n");
        }

        em.close();

        emf.close();
    }
}
