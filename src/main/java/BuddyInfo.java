import javax.persistence.*;

@Entity
public class BuddyInfo {
    @Id
    private int id;
    private String name;
    private String phoneNumber;
    @ManyToOne
    private AddressBook addressBook;

    public BuddyInfo() {
        this.name = null;
        this.phoneNumber = null;
    }

    public BuddyInfo(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public AddressBook getAddressBook() {
        return addressBook;
    }

    public void setAddressBook(AddressBook addressBook) {
        this.addressBook = addressBook;
    }
    @Override
    public String toString() {
        return "Name: " + getName() + " Phone Number: " + getPhoneNumber() + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BuddyInfo)) return false;
        BuddyInfo b = (BuddyInfo) o;
        return b.getName().equals(this.getName()) && b.getPhoneNumber().equals(this.getPhoneNumber());
    }
}
