import javax.swing.*;
import java.awt.event.ActionListener;

public abstract class AddressBookActionListener implements ActionListener {
    protected AddressBook addressBook;
    protected JTextArea textArea;

    public void setAddressBook(AddressBook ab) {
        this.addressBook = ab;
    }

    public void setTextArea(JTextArea textArea) {
        this.textArea = textArea;
    }
}
